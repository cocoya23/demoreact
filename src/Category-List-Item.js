import React, {Component} from 'react';

class CategoryListItem extends Component {
	
	render() {
		return (
			<li>
				{this.props.name}
				<button onClick={() => this.props.deleteItem(this.props.name)}>X</button>
			</li>
		)
	}
	
}

export default CategoryListItem;