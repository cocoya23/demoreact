import React, {Component} from 'react';
import CategoryItem from './Category-List-Item';
import request from 'request';

class CategoryList extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			items: [],
			newItem: ""
		}
	}
	
	componentDidMount() {
		this.downloadCountries();
	}
	
	downloadCountries() {
		request({
			uri: 'https://api.docademic.com/api/catalog/country',
			method: 'GET',
			json: true
		},(err,body,response) => {
			let items = response.data.countries.map((country) => {
				return country.name;
			});
			this.setState({items:items});
		});
	}
	
	renderItems() {
		let htmlItems = this.state.items.map((item) => {
			return <CategoryItem key={item} name={item} deleteItem={(name) => this.deleteItem(name)}/>
		});
		return htmlItems;
	}
	
	addItem(event) {
		event.preventDefault();
		let items = this.state.items;
		items.push(this.state.newItem);
		this.setState({items:items,newItem:""});
	}
	
	deleteItem(name) {
		let items = this.state.items.filter((item) => {
			if(item !== name) return item;
		});
		this.setState({items:items});
	}
	
	render() {
		return (
			<div>
				<form onSubmit={(event)=> this.addItem(event)}>
					<input type="text" value={this.state.newItem} onChange={(event) => {this.setState({newItem:event.target.value})}}/>
				</form>
				<ul>
					{this.renderItems()}
				</ul>
			</div>
		)
	}
	
}
export default CategoryList;